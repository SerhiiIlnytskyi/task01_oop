package com.epam.courses.controller.impl;

import com.epam.courses.controller.AirlineController;
import com.epam.courses.model.Aircraft;
import com.epam.courses.model.businesslogic.BusinessLogic;
import com.epam.courses.model.businesslogic.Model;
import java.util.List;

public class AirlineControllerImpl implements AirlineController {

    private Model model;

    public AirlineControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public Integer getTotalPassengerCapacity() {
        return model.getTotalPassengerCapacity();
    }

    @Override
    public List<Aircraft> getAircraftsByFuelConsumption(Integer fromConsumption,
        Integer toConsumption) {
        return model.getAircraftsByFuelConsumption(fromConsumption, toConsumption);
    }

    @Override
    public Double getTotalCarryingCapacity() {
        return model.getTotalCarryingCapacity();
    }

    @Override
    public List<Aircraft> getSortedAircraftsByMaximumFlightRange() {
        return model.getSortedAircraftsByMaximumFlightRange();
    }
}
