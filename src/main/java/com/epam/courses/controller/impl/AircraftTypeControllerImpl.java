package com.epam.courses.controller.impl;

import com.epam.courses.controller.AircraftTypeController;
import com.epam.courses.model.AircraftType;
import com.epam.courses.model.businesslogic.BusinessLogic;
import com.epam.courses.model.businesslogic.Model;
import java.util.List;

public class AircraftTypeControllerImpl implements AircraftTypeController {

    private Model model;

    public AircraftTypeControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public List<AircraftType> getAircraftTypeList() {
        return model.getAllAircraftTypes();
    }

    @Override
    public void createNewAircraftType(String type, String aircraftModel, Double maximumFlightRange,
        Integer passengerCapacity, Double carryingCapacity,
        Double fuelConsumption) {
        model.addNewAircraftType(type, aircraftModel, maximumFlightRange, passengerCapacity,
            carryingCapacity, fuelConsumption);
    }
}
