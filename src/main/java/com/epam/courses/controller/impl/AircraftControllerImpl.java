package com.epam.courses.controller.impl;

import com.epam.courses.controller.AircraftController;
import com.epam.courses.model.Aircraft;
import com.epam.courses.model.businesslogic.BusinessLogic;
import com.epam.courses.model.businesslogic.Model;

public class AircraftControllerImpl implements AircraftController {

    private Model model;

    public AircraftControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public void createAircraft(Integer aircraftTypeNumber) {
        model.addAircraft(new Aircraft(model.getAllAircraftTypes().get(aircraftTypeNumber)));
    }
}
