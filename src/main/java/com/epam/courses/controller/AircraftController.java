package com.epam.courses.controller;

public interface AircraftController {
    void createAircraft(Integer aircraftTypeNumber);
}
