package com.epam.courses.controller;

import com.epam.courses.model.Aircraft;
import java.util.List;

public interface AirlineController {

    Integer getTotalPassengerCapacity();
    List<Aircraft> getAircraftsByFuelConsumption(Integer fromConsumption, Integer toConsumption);
    Double getTotalCarryingCapacity();
    List<Aircraft> getSortedAircraftsByMaximumFlightRange();
}
