package com.epam.courses.controller;

import com.epam.courses.model.AircraftType;

import java.util.List;

public interface AircraftTypeController {
    List<AircraftType> getAircraftTypeList();
    void  createNewAircraftType(String type, String model, Double maximumFlightRange, Integer passengerCapacity,
                                Double carryingCapacity, Double fuelConsumption);
}
