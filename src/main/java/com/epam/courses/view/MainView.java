package com.epam.courses.view;

import com.epam.courses.controller.AircraftController;
import com.epam.courses.controller.AircraftTypeController;
import com.epam.courses.controller.AirlineController;
import com.epam.courses.controller.impl.AircraftControllerImpl;
import com.epam.courses.controller.impl.AircraftTypeControllerImpl;
import com.epam.courses.controller.impl.AirlineControllerImpl;
import com.epam.courses.model.AircraftType;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MainView {

    private static Scanner INPUT = new Scanner(System.in);
    private AircraftController aircraftController;
    private AircraftTypeController aircraftTypeController;
    private AirlineController airlineController;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MainView() {
        aircraftController = new AircraftControllerImpl();
        aircraftTypeController = new AircraftTypeControllerImpl();
        airlineController = new AirlineControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - show all aircraft types");
        menu.put("2", " 2 - add new aircraft type");
        menu.put("3", " 3 - create new aircraft");
        menu.put("4", " 4 - calculate total passenger capacity");
        menu.put("5", " 5 - calculate total carrying capacity");
        menu.put("6", " 6 - get all aircrafts by range of fuel consumption");
        menu.put("7", " 7 - get aircrafts sorted by maximum flight range");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
    }

    private void pressButton1() {
        aircraftTypeController.getAircraftTypeList().forEach(System.out::println);
    }

    private void pressButton2() {
        System.out.print("Please write type of aircraft(plane, helicopter, etc): ");
        String type = INPUT.nextLine();
        System.out.print("Please write model of aircraft(boeing 737, etc): ");
        String model = INPUT.nextLine();
        System.out.print("Please write maximumFlightRange: ");
        Double maximumFlightRange = INPUT.nextDouble();
        System.out.print("Please write passengerCapacity: ");
        Integer passengerCapacity = INPUT.nextInt();
        System.out.print("Please write carryingCapacity: ");
        Double carryingCapacity = INPUT.nextDouble();
        System.out.print("Please write fuelConsumption: ");
        Double fuelConsumption = INPUT.nextDouble();
        aircraftTypeController
            .createNewAircraftType(type, model, maximumFlightRange, passengerCapacity,
                carryingCapacity, fuelConsumption);
    }

    private void pressButton3() {
        System.out.println("Avalible aircrafts types:");
        List<AircraftType> aircraftTypes = aircraftTypeController.getAircraftTypeList();
        for (int i = 0; i < aircraftTypes.size(); i++) {
            System.out.println(i + ". " + aircraftTypes.get(i));
        }
        System.out.print("Please choose a number of aircraftType:");
        Integer aircraftTypeNumber = INPUT.nextInt();
        aircraftController.createAircraft(aircraftTypeNumber);
    }

    private void pressButton4() {
        System.out
            .println("Total passenger capacity: " + airlineController.getTotalPassengerCapacity());
    }

    private void pressButton5() {
        System.out
            .println("Total carrying capacity: " + airlineController.getTotalCarryingCapacity());
    }

    private void pressButton6() {
        System.out.print("Please write fromConsumption: ");
        Integer fromConsumption = INPUT.nextInt();
        System.out.print("Please write toConsumption: ");
        Integer toConsumption = INPUT.nextInt();
        airlineController.getAircraftsByFuelConsumption(fromConsumption, toConsumption)
            .forEach(System.out::println);
    }

    private void pressButton7() {
        airlineController.getSortedAircraftsByMaximumFlightRange()
            .forEach(System.out::println);
    }

    private void pressButton0() {
        System.out.println("Good luck");

    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println(" Q - quit");
            System.out.print("Please, select menu point: ");
            keyMenu = INPUT.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        INPUT.close();
        System.exit(0);
    }
}

