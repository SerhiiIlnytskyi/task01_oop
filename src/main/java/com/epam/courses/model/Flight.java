package com.epam.courses.model;

import java.time.LocalDateTime;

public class Flight {

    private Integer flightNumber;
    private Aircraft aircraft;
    private String depparturePort;
    private String arrivalPort;
    private LocalDateTime scheduledDepparture;
    private LocalDateTime estimatedDepparture;
    private LocalDateTime scheduledArrival;
    private LocalDateTime estimatedArrival;

    public Flight(Integer flightNumber, Aircraft aircraft, String depparturePort,
        String arrivalPort,
        LocalDateTime scheduledDepparture, LocalDateTime estimatedDepparture,
        LocalDateTime scheduledArrival, LocalDateTime estimatedArrival) {
        this.flightNumber = flightNumber;
        this.aircraft = aircraft;
        this.depparturePort = depparturePort;
        this.arrivalPort = arrivalPort;
        this.scheduledDepparture = scheduledDepparture;
        this.estimatedDepparture = estimatedDepparture;
        this.scheduledArrival = scheduledArrival;
        this.estimatedArrival = estimatedArrival;
    }


    public Integer getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(Integer flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    public String getDepparturePort() {
        return depparturePort;
    }

    public void setDepparturePort(String depparturePort) {
        this.depparturePort = depparturePort;
    }

    public String getArrivalPort() {
        return arrivalPort;
    }

    public void setArrivalPort(String arrivalPort) {
        this.arrivalPort = arrivalPort;
    }

    public LocalDateTime getScheduledDepparture() {
        return scheduledDepparture;
    }

    public void setScheduledDepparture(LocalDateTime scheduledDepparture) {
        this.scheduledDepparture = scheduledDepparture;
    }

    public LocalDateTime getEstimatedDepparture() {
        return estimatedDepparture;
    }

    public void setEstimatedDepparture(LocalDateTime estimatedDepparture) {
        this.estimatedDepparture = estimatedDepparture;
    }

    public LocalDateTime getScheduledArrival() {
        return scheduledArrival;
    }

    public void setScheduledArrival(LocalDateTime scheduledArrival) {
        this.scheduledArrival = scheduledArrival;
    }

    public LocalDateTime getEstimatedArrival() {
        return estimatedArrival;
    }

    public void setEstimatedArrival(LocalDateTime estimatedArrival) {
        this.estimatedArrival = estimatedArrival;
    }

    @Override
    public String toString() {
        return "Flight{" +
            "flightNumber=" + flightNumber +
            ", aircraft=" + aircraft +
            ", depparturePort='" + depparturePort + '\'' +
            ", arrivalPort='" + arrivalPort + '\'' +
            ", scheduledDepparture=" + scheduledDepparture +
            ", estimatedDepparture=" + estimatedDepparture +
            ", scheduledArrival=" + scheduledArrival +
            ", estimatedArrival=" + estimatedArrival +
            '}';
    }
}
