package com.epam.courses.model;

public abstract class StaffMember {

    private String name;
    private Integer experience;

    public StaffMember(String name) {
        this.name = name;
        this.experience = 0;
    }

    public StaffMember(String name, Integer experience) {
        this.name = name;
        this.experience = experience;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

}
