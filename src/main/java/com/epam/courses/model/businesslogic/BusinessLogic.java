package com.epam.courses.model.businesslogic;

import com.epam.courses.model.Aircraft;
import com.epam.courses.model.AircraftType;
import com.epam.courses.model.Airline;
import com.epam.courses.model.StaffMember;
import java.util.ArrayList;
import java.util.List;

public class BusinessLogic implements Model {

    private static Airline airline;
    private static List<AircraftType> aircraftTypes = new ArrayList<>();
    private static List<Aircraft> aircrafts = new ArrayList<>();
    private static List<StaffMember> staffMembers = new ArrayList<>();

    public BusinessLogic() {
        airline = new Airline();
    }

    @Override
    public void addNewAircraftType(String type, String model, Double maximumFlightRange,
        Integer passengerCapacity,
        Double carryingCapacity, Double fuelConsumption) {
        aircraftTypes.add(new AircraftType(type, model, maximumFlightRange, passengerCapacity,
            carryingCapacity, fuelConsumption));
    }

    @Override
    public List<AircraftType> getAllAircraftTypes() {
        return aircraftTypes;
    }

    @Override
    public void addAircraft(Aircraft aircraft) {
        this.airline.addAircraft(aircraft);
        this.aircrafts.add(aircraft);
    }

    @Override
    public Integer getTotalPassengerCapacity() {
        return airline.totalPassangerCapacity();
    }

    @Override
    public List<Aircraft> getAircraftsByFuelConsumption(Integer fromConsumption,
        Integer toConsumption) {
        return airline.findAircraftsByFuelConsumption(fromConsumption, toConsumption);
    }

    @Override
    public Double getTotalCarryingCapacity() {
        return airline.totalCarryingCapacity();
    }

    @Override
    public List<Aircraft> getSortedAircraftsByMaximumFlightRange() {
        return airline.sortAircraftsByMaximumFlightRange();
    }
}
