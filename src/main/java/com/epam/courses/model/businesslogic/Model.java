package com.epam.courses.model.businesslogic;

import com.epam.courses.model.Aircraft;
import com.epam.courses.model.AircraftType;
import java.util.List;

public interface Model {

    List<AircraftType> getAllAircraftTypes();
    void addNewAircraftType(String type, String model, Double maximumFlightRange,
        Integer passengerCapacity,
        Double carryingCapacity, Double fuelConsumption);
    void addAircraft(Aircraft aircraft);
    Integer getTotalPassengerCapacity();
    List<Aircraft> getAircraftsByFuelConsumption(Integer fromConsumption, Integer toConsumption);
    Double getTotalCarryingCapacity();
    List<Aircraft> getSortedAircraftsByMaximumFlightRange();
}
