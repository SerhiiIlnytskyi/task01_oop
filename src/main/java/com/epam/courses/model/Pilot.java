package com.epam.courses.model;

public class Pilot extends StaffMember {

    public Pilot(String name) {
        super(name);
    }

    public Pilot(String name, Integer experience) {
        super(name, experience);
    }


    @Override
    public String toString() {
        return "Pilot{" +
            "name='" + this.getName() + '\'' +
            ", experience=" + this.getExperience() +
            '}';
    }
}
