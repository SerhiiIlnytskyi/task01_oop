package com.epam.courses.model;

public class Attendant extends StaffMember {

    public Attendant(String name) {
        super(name);
    }

    public Attendant(String name, Integer experience) {
        super(name, experience);
    }

    @Override
    public String toString() {
        return "Attendant{" +
            "name='" + this.getName() + '\'' +
            ", experience=" + this.getExperience() +
            '}';
    }
}
