package com.epam.courses.model;

import java.util.List;

public class Aircraft {

    private AircraftType aircraftType;
    private Boolean working;
    private Pilot capitan;
    private List<Pilot> pilots;
    private Attendant headAttendant;
    private List<Attendant> attendants;

    public Aircraft() {
    }

    public Aircraft(AircraftType aircraftType) {
        this.aircraftType = aircraftType;
    }

    public Boolean canFlight() {
        int totalPilotsExperience = 0;
        int totalAttendantsExperience = 0;
        for (int i = 0; i < pilots.size(); i++) {
            totalPilotsExperience += pilots.get(i).getExperience();
        }
        for (int i = 0; i < attendants.size(); i++) {
            totalAttendantsExperience += attendants.get(i).getExperience();
        }
        return (pilots.size() >= aircraftType.getMinimumNumberOfAttendants()) &&
            (attendants.size() >= aircraftType.getMinimumNumberOfAttendants()) &&
            (totalPilotsExperience >= aircraftType.getMinimumTotalPilotsExperience()) &&
            (totalAttendantsExperience >= aircraftType.getMinimumTotalAttendantsExperience());
    }

    public AircraftType getAircraftType() {
        return aircraftType;
    }

    public void setAircraftType(AircraftType aircraftType) {
        this.aircraftType = aircraftType;
    }

    public Boolean getWorking() {
        return working;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public Pilot getCapitan() {
        return capitan;
    }

    public void setCapitan(Pilot capitan) {
        this.capitan = capitan;
        this.pilots.add(capitan);
    }

    public List<Pilot> getPilots() {
        return pilots;
    }

    public void setPilots(List<Pilot> pilots) {
        this.pilots = pilots;
    }

    public void addPilot(Pilot pilot) {
        this.pilots.add(pilot);
    }

    public Attendant getHeadAttendant() {
        return headAttendant;
    }

    public void setHeadAttendant(Attendant headAttendant) {
        this.headAttendant = headAttendant;
        this.attendants.add(headAttendant);
    }

    public List<Attendant> getAttendants() {
        return attendants;
    }

    public void setAttendants(List<Attendant> attendants) {
        this.attendants = attendants;
    }

    public void addAttendant(Attendant attendant) {
        this.attendants.add(attendant);
    }

    @Override
    public String toString() {
        return "Aircraft{" +
            "aircraftType=" + aircraftType +
            ", working=" + working +
            ", capitan=" + capitan +
            ", pilots=" + pilots +
            ", headAttendant=" + headAttendant +
            ", attendants=" + attendants +
            '}';
    }
}
