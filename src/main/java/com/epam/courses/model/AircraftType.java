package com.epam.courses.model;

public class AircraftType {

    private String type;
    private String model;
    private Integer minimumNumberOfPilots;
    private Integer minimumNumberOfAttendants;
    private Integer minimumTotalPilotsExperience;
    private Integer minimumTotalAttendantsExperience;
    private Double maximumFlightRange;
    private Integer passengerCapacity;
    private Double carryingCapacity;
    private Double fuelConsumption;
    private Double averageSpeed;

    public AircraftType(String type, String model, Double maximumFlightRange,
        Integer passengerCapacity,
        Double carryingCapacity, Double fuelConsumption) {
        this.type = type;
        this.model = model;
        this.maximumFlightRange = maximumFlightRange;
        this.passengerCapacity = passengerCapacity;
        this.carryingCapacity = carryingCapacity;
        this.fuelConsumption = fuelConsumption;
    }

    public AircraftType(String type, String model, Integer minimumNumberOfPilots,
        Integer minimumNumberOfAttendants,
        Double maximumFlightRange, Integer passengerCapacity, Double carryingCapacity,
        Double fuelConsumption, Double averageSpeed) {
        this.type = type;
        this.model = model;
        this.minimumNumberOfPilots = minimumNumberOfPilots;
        this.minimumNumberOfAttendants = minimumNumberOfAttendants;
        this.maximumFlightRange = maximumFlightRange;
        this.passengerCapacity = passengerCapacity;
        this.carryingCapacity = carryingCapacity;
        this.fuelConsumption = fuelConsumption;
        this.averageSpeed = averageSpeed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getMinimumNumberOfPilots() {
        return minimumNumberOfPilots;
    }

    public void setMinimumNumberOfPilots(Integer minimumNumberOfPilots) {
        if (minimumNumberOfPilots > 0) {
            this.minimumNumberOfPilots = minimumNumberOfPilots;
        } else {
            this.minimumNumberOfAttendants = 0;
        }
    }

    public Integer getMinimumNumberOfAttendants() {
        return minimumNumberOfAttendants;
    }

    public void setMinimumNumberOfAttendants(Integer minimumNumberOfAttendants) {
        if (minimumNumberOfAttendants > 0) {
            this.minimumNumberOfAttendants = minimumNumberOfAttendants;
        } else {
            this.minimumNumberOfAttendants = 0;
        }
    }

    public Double getMaximumFlightRange() {
        return maximumFlightRange;
    }

    public Integer getMinimumTotalPilotsExperience() {
        return minimumTotalPilotsExperience;
    }

    public void setMinimumTotalPilotsExperience(Integer minimumTotalPilotsExperience) {
        this.minimumTotalPilotsExperience = minimumTotalPilotsExperience;
    }

    public Integer getMinimumTotalAttendantsExperience() {
        return minimumTotalAttendantsExperience;
    }

    public void setMinimumTotalAttendantsExperience(Integer minimumTotalAttendantsExperience) {
        this.minimumTotalAttendantsExperience = minimumTotalAttendantsExperience;
    }

    public void setMaximumFlightRange(Double maximumFlightRange) {
        if (maximumFlightRange > 0) {
            this.maximumFlightRange = maximumFlightRange;
        } else {
            this.maximumFlightRange = .0;
        }
    }

    public Integer getPassengerCapacity() {
        return passengerCapacity;
    }

    public void setPassengerCapacity(Integer passengerCapacity) {
        if (passengerCapacity > 0) {
            this.passengerCapacity = passengerCapacity;
        } else {
            this.passengerCapacity = 0;
        }
    }

    public Double getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(Double carryingCapacity) {
        if (carryingCapacity > 0) {
            this.carryingCapacity = carryingCapacity;
        } else {
            this.carryingCapacity = .0;
        }

    }

    public Double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(Double fuelConsumption) {
        if (fuelConsumption > 0) {
            this.fuelConsumption = fuelConsumption;
        } else {
            this.fuelConsumption = .0;
        }
    }

    public Double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(Double averageSpeed) {
        if (averageSpeed > 0) {
            this.averageSpeed = averageSpeed;
        } else {
            this.averageSpeed = .0;
        }
    }

    @Override
    public String toString() {
        return "AircraftType{" +
            "type='" + type + '\'' +
            ", model='" + model +
            '}';
    }
}
