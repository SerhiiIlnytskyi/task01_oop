package com.epam.courses.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Airline {

    private String name;
    private List<Flight> flights = new ArrayList<>();
    private List<Aircraft> aircrafts = new ArrayList<>();
    private List<StaffMember> staffMembers = new ArrayList<>();

    public Airline() {
    }

    public Airline(String name) {
        this.name = name;
    }

    public Airline(String name, List<Aircraft> aircrafts, List<StaffMember> staffMembers) {
        this.name = name;
        this.aircrafts = aircrafts;
        this.staffMembers = staffMembers;
    }

    public Integer totalPassangerCapacity() {
        Integer capacity = 0;
        for (int i = 0; i < aircrafts.size(); i++) {
            capacity += aircrafts.get(i).getAircraftType().getPassengerCapacity();
        }
        return capacity;
    }

    public Double totalCarryingCapacity() {
        Double carrying = .0;
        for (int i = 0; i < aircrafts.size(); i++) {
            carrying += aircrafts.get(i).getAircraftType().getCarryingCapacity();
        }
        return carrying;
    }

    public List<Aircraft> sortAircraftsByMaximumFlightRange() {
        List<Aircraft> sortedAircrafts = this.aircrafts;
        sortedAircrafts.sort((o1, o2) ->
            (int) (o1.getAircraftType().getMaximumFlightRange() - o2.getAircraftType()
                .getMaximumFlightRange()));
        return sortedAircrafts;
    }

    public List<Aircraft> findAircraftsByFuelConsumption(Integer fromConsumption,
        Integer toConsumption) {
        return this.aircrafts.stream()
            .filter(
                aircraft -> (aircraft.getAircraftType().getFuelConsumption() > fromConsumption) &&
                    (aircraft.getAircraftType().getFuelConsumption() < toConsumption))
            .collect(Collectors.toList());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    public List<Aircraft> getAircrafts() {
        return aircrafts;
    }

    public void setAircrafts(List<Aircraft> aircrafts) {
        this.aircrafts = aircrafts;
    }

    public void addAircraft(Aircraft aircraft) {
        this.aircrafts.add(aircraft);
    }

    public List<StaffMember> getStaffMembers() {
        return staffMembers;
    }

    public void setStaffMembers(List<StaffMember> staffMembers) {
        this.staffMembers = staffMembers;
    }

    @Override
    public String toString() {
        return "Airline{" +
            "name='" + name + '\'' +
            ", flights=" + flights +
            ", aircrafts=" + aircrafts +
            ", staffMembers=" + staffMembers +
            '}';
    }
}
