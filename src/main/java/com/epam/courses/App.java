package com.epam.courses;

import com.epam.courses.view.MainView;

public class App {

    public static void main(String[] args) {
        new MainView().show();
    }
}
